const inputText = document.querySelector("#new-message-text");

inputText.addEventListener("click",() => {
  if (inputText.firstChild) {
    inputText.removeChild(inputText.firstChild);
    inputText.focus()
  }
})
